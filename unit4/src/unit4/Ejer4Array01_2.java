package unit4;

public class Ejer4Array01_2 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[][] array;
		boolean var = true;
		array = new int[10][10];
		for (int row = 0; row < 10; row++) {
			for (int col = 0; col < 10; col++) {
				array[row][col] = 0;
			}
		}
		array[0][4] = 1;
		array[2][6] = 1;
		array[3][1] = 1;
		array[8][6] = 1;

		for (int row = 0; row < 10; row++) {
			var = true;
			System.out.println("");
			for (int col = 0; col < 10; col++) {
				if (var) {
					System.out.print(array[row][col]);
				} else {
					System.out.print(" " + array[row][col]);
				}
				var = false;
			}
		}
		int numrow = 0;
		int numcol = 0;
		boolean var2 = true;
		boolean var3 = true;
		// mirar cuantas filas con 0 hay
		for (int row = 0; row < array.length; row++) {
			var2 = true;
			for (int col = 0; col < array[0].length; col++) {
				if (array[row][col] == 1) {
					var2 = false;
				}
			}
			if (var2) {
				numrow++;
			}
		}
		System.out.println("\n There are " + numrow + " rows with all 0");

		// mirar cuantas columnas tienen todo 0
		for (int col = 0; col < array.length; col++) {
			var3 = true;
			for (int row = 0; row < array[0].length; row++) {
				if (array[row][col] == 1) {
					var3 = false;
				}
			}
			if (var3) {
				numcol++;
			}
		}
		System.out.println("\n There are " + numcol + " rows with all 0");
	}

}
