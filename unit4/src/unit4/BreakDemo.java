package unit4;

import java.util.Scanner;

public class BreakDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter a value");
		boolean prime = true;
		int n;
		Scanner inputValue;
		inputValue = new Scanner(System.in);
		n = inputValue.nextInt();
		for (int i = 2; i < n; i++) {
			if (n % i == 0) {
				prime = false;
				break;
			}
		}
		if (prime)
			System.out.println("Prime");
		else
			System.out.println("NOT Prime");
	}
}
