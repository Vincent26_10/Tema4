package unit4;

public class Ejer3Array01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[][] array;
		boolean var = true;
		array = new int[10][10];
		for (int row = 0; row < 10; row++) {
			for (int col = 0; col < 10; col++) {
				array[row][col] = 0;
			}
		}
		array[0][4] = 1;
		array[2][6] = 1;
		array[3][1] = 1;
		array[8][6] = 1;

		for (int row = 0; row < 10; row++) {
			var = true;
			System.out.println("");
			for (int col = 0; col < 10; col++) {
				if (var) {
					System.out.print(array[row][col]);
				} else {
					System.out.print(" " + array[row][col]);
				}
				var = false;
			}
		}
		
		

	}

}
