package unit4;

public class Main2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bicycle bike1 = new Bicycle();
		Bicycle bike2 = new Bicycle();
		bike1.changeCadence(50);
		bike1.changeSpeedUp(10);
		bike1.changeGear(2);
		bike1.printStates();
		bike2.changeCadence(50);
		bike2.changeSpeedUp(10);
		bike2.changeGear(2);
		bike2.changeCadence(40);
		bike2.changeSpeedUp(10);
		bike2.changeGear(3);
		bike2.printStates();

	}

}
