package unit4;


	public class TamdemBike extends Bicycle{
		private int numSeats = 2;
		
		void applyBreaks(int decrement1, int decrement2) {
			applyBrakes(decrement1);
			applyBrakes(decrement2);
			
		}

		
		void printStates() {
			super.printStates();
			System.out.println("num seats = " +numSeats);
		}
	}


