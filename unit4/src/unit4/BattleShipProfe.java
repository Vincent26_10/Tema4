package unit4;
import java.util.Scanner;

public class BattleShipProfe {
	public static final int NUM_SHIPS = 10;
	public static final int DIMENSION = 8;
	static boolean gameOver;

	public static void main(String[] args) {
		gameOver = false;
		char[][] matrix = new char[DIMENSION][DIMENSION+1];
		initMatrix(matrix);
		addShips(matrix);
		//printMatrix(matrix);
		//printMatrix2(matrix);
		
		Scanner input = new Scanner(System.in);
		int number;
		char letter;
		int row;
		int cont=0;
		while(!gameOver) {
			printMatrix2(matrix);
			System.out.println("Tell me a letter");
			letter=input.next().charAt(0); 
			System.out.println("Tell me a number");
			number=input.nextInt();
			row = letter - 'A';
			if(matrix[row][number]=='1') {
				matrix[row][number]='X';
				cont++;
			}else {
				matrix[row][number]='O';
			}
			if(cont>=NUM_SHIPS) {
				gameOver =true;
				System.out.println("El juego ha terminado");
			}
		}
		
	}
	private static void printMatrix(char[][] m) {
		printHeader();
		printMatrixBody(m);
	}
	private static void printMatrix2(char[][] m) {
		printHeader();
		printMatrixBody2(m);
	}
	

	private static void printHeader() {
		System.out.print("  ");
		for(int i=1; i<=8;i++) {
			System.out.print(i+" ");
		}
		System.out.println();
		
	}
	
	public static void printMatrixBody(char[][] m) {
		char c = 'A';

		for (int row = 0; row < m.length; row++) {
			System.out.print(c+" ");
			c++;
			for (int col = 1; col < m[0].length; col++) {

				
					System.out.print(m[row][col]+" ");
				
			}

			System.out.println();

		}
		

	}
	public static void printMatrixBody2(char[][] m) {
		char c = 'A';

		for (int row = 0; row < m.length; row++) {
			System.out.print(c+" ");
			c++;
			for (int col = 1; col < m[0].length; col++) {

				if(m[row][col]=='1') {
					System.out.print('.'+" ");
				}else {
					System.out.print(m[row][col]+" ");
				}
				
			}

			System.out.println();

		}
		

	}

	private static void addShips(char[][] m) {
		int cont = 0;
		while (cont < 10) {
			int x = (int) (Math.random() * (m.length - 1) + 1);
			int y = (int) (Math.random() * (m.length - 1) + 1);
			if (m[x][y] == '.') {
				m[x][y] = '1';
				cont++;
			}

		}

	}

	
	 

	public static void initMatrix(char[][] m) {
		for (int row = 0; row < m.length; row++) {
			for (int col = 0; col < m[0].length; col++) {
				m[row][col] = '.';
			}

		}

	}

}
